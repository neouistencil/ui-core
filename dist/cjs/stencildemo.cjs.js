'use strict';

const core = require('./core-d722da7d.js');

core.patchBrowser().then(options => {
  return core.bootstrapLazy([["rh-button_2.cjs",[[1,"rh-button",{"name":[1]}],[1,"rh-switch",{"name":[1]}]]]], options);
});
