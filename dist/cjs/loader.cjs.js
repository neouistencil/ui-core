'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const core = require('./core-d722da7d.js');

const defineCustomElements = (win, options) => {
  return core.patchEsm().then(() => {
    core.bootstrapLazy([["rh-button_2.cjs",[[1,"rh-button",{"name":[1]}],[1,"rh-switch",{"name":[1]}]]]], options);
  });
};

exports.defineCustomElements = defineCustomElements;
