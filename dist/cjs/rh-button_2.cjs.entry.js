'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const core = require('./core-d722da7d.js');

const RhButton = class {
    constructor(hostRef) {
        core.registerInstance(this, hostRef);
    }
    render() {
        return (core.h("div", null, core.h("button", { type: "button", class: "btn cta-btn" }, core.h("span", { class: "button-label" }, this.name), core.h("span", { class: 'icon icon-next' }))));
    }
    static get style() { return ".btn{height:44px;display:-ms-flexbox;display:flex;-ms-flex-align:center;align-items:center;position:relative;padding:13px 0;margin:3;min-width:auto;font-size:1.25rem;text-decoration:none;vertical-align:middle;letter-spacing:0;cursor:pointer;background-color:transparent;border:0;border-radius:0;outline:0;will-change:box-shadow,transform}.btn:hover{color:#fff}.btn:hover:after{width:100%;-webkit-transition:width .2s linear;transition:width .2s linear}.btn:after{position:absolute;content:\"\";top:0;left:0;width:0;height:100%;background-color:#000;-webkit-transform-origin:left;transform-origin:left;z-index:-1}.btn:active,.btn:focus{outline:none;-webkit-box-shadow:none!important;box-shadow:none!important;background-color:#000;color:#fff}.btn:active .icon,.btn:focus .icon{background-color:#000}.btn:active .icon:before,.btn:focus .icon:before{color:#fff}.btn:active .icon:after,.btn:focus .icon:after{width:100%;-webkit-transition:width .2s linear;transition:width .2s linear}.btn:active:focus{-webkit-box-shadow:none!important;box-shadow:none!important}.cta-btn{color:#fff}.cta-btn,.cta-btn .icon{background-color:#dd0060}.cta-btn .icon:before{color:#fff}.cta-btn:active,.cta-btn:focus{color:#fff;background-color:#000}.button-label{margin:0 8px 0 16px;line-height:1;text-transform:capitalize}.icon{height:44px;width:44px;left:100%;display:-ms-flexbox;display:flex;-ms-flex-align:center;align-items:center;-ms-flex-pack:center;justify-content:center;will-change:box-shadow,transform;-webkit-transition:color .3s linear,width .3s linear;transition:color .3s linear,width .3s linear}.icon,.icon:after{position:absolute;top:0}.icon:after{content:\"\";left:0;width:0;height:100%;background-color:#000;-webkit-transform-origin:left;transform-origin:left;z-index:-1}.icon:active,.icon:focus{-webkit-box-shadow:none;box-shadow:none}.icon:active:after,.icon:focus:after{width:100%;-webkit-transition:width .2s linear;transition:width .2s linear}"; }
};

const RhSwitch = class {
    constructor(hostRef) {
        core.registerInstance(this, hostRef);
    }
    render() {
        return (core.h("div", null, core.h("label", { class: "switch" }, core.h("input", { type: "checkbox", checked: true, "data-checked": "On", "data-unchecked": "Off" }), core.h("span", { class: "slider" }))));
    }
    static get style() { return ".switch{position:relative;display:inline-block;width:80px;height:40px;border:1px solid grey}.switch input{width:0;height:0}.switch input:checked+.slider{background-color:#fff}.switch input:checked+.slider:before{background-color:#dd0060;-webkit-transform:translateX(100%);-ms-transform:translateX(100%);transform:translateX(100%)}.switch input:checked:after{color:#fff}.switch input:checked:before{color:#b1b2b3}.switch input:before{content:attr(data-unchecked);color:#fff;left:8px}.switch input:after,.switch input:before{position:absolute;top:50%;-webkit-transform:translateY(-50%);transform:translateY(-50%);z-index:1;font-size:14px;font-weight:700;line-height:1.71;text-transform:uppercase}.switch input:after{content:attr(data-checked);color:#b1b2b3;right:8px}.switch .slider{cursor:pointer;top:0;right:0;background-color:#fff}.switch .slider,.switch .slider:before{position:absolute;left:0;bottom:0;-webkit-transition:.4s;transition:.4s}.switch .slider:before{content:\"\";height:100%;width:50%;background-color:#000}"; }
};

exports.rh_button = RhButton;
exports.rh_switch = RhSwitch;
