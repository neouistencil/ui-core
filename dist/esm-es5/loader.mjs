import { a as patchEsm, b as bootstrapLazy } from './core-dfb308f5.js';
var defineCustomElements = function (win, options) {
    return patchEsm().then(function () {
        bootstrapLazy([["rh-button_2", [[1, "rh-button", { "name": [1] }], [1, "rh-switch", { "name": [1] }]]]], options);
    });
};
export { defineCustomElements };
