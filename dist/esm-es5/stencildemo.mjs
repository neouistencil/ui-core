import { p as patchBrowser, b as bootstrapLazy } from './core-dfb308f5.js';
patchBrowser().then(function (options) {
    return bootstrapLazy([["rh-button_2", [[1, "rh-button", { "name": [1] }], [1, "rh-switch", { "name": [1] }]]]], options);
});
