import { h } from "@stencil/core";
export class RhSwitch {
    render() {
        return (h("div", null,
            h("label", { class: "switch" },
                h("input", { type: "checkbox", checked: true, "data-checked": "On", "data-unchecked": "Off" }),
                h("span", { class: "slider" }))));
    }
    static get is() { return "rh-switch"; }
    static get encapsulation() { return "shadow"; }
    static get originalStyleUrls() { return {
        "$": ["rh-switch.scss"]
    }; }
    static get styleUrls() { return {
        "$": ["rh-switch.css"]
    }; }
    static get properties() { return {
        "name": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": "The first name"
            },
            "attribute": "name",
            "reflect": false
        }
    }; }
}
