import { h } from "@stencil/core";
export class RhButton {
    render() {
        return (h("div", null,
            h("button", { type: "button", class: "btn cta-btn" },
                h("span", { class: "button-label" }, this.name),
                h("span", { class: 'icon icon-next' }))));
    }
    static get is() { return "rh-button"; }
    static get encapsulation() { return "shadow"; }
    static get originalStyleUrls() { return {
        "$": ["rh-button.scss"]
    }; }
    static get styleUrls() { return {
        "$": ["rh-button.css"]
    }; }
    static get properties() { return {
        "name": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": "The first name"
            },
            "attribute": "name",
            "reflect": false
        }
    }; }
}
