import { Component, Prop, h } from '@stencil/core'

@Component({
  tag: 'rh-switch',
  styleUrl: 'rh-switch.scss',
  shadow: true,
})
export class RhSwitch {
  /**
   * The first name
   */
  @Prop()
  name: string

  render() {
    return (
      <div>
        <label class="switch">
          <input type="checkbox" checked data-checked="On" data-unchecked="Off" />
          <span class="slider"></span>
        </label>
      </div >
    )
  }
}