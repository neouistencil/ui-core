import { Component, Prop, h } from '@stencil/core'

@Component({
  tag: 'rh-button',
  styleUrl: 'rh-button.scss',
  shadow: true,
})
export class RhButton {
  /**
   * The first name
   */
  @Prop()
  name: string

  render() {
    return (
      <div>
        <button type="button" class="btn cta-btn">
          <span class="button-label">{this.name}</span>
          <span class='icon icon-next'></span>
        </button>
      </div >
    )
  }
}